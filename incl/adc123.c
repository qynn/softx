/******************************************************************************
 * file    : adc123.c
 * target  : ATtiny85 (Int. Osc. @8MHz  w/ CKDIV8=1)
 * author  : .qusw (https://github.com/qusvv) 
 * license : GNU GPLv3
 * 
 * # # AnalogRead() for ATtiny85 # # 
 * 
 *  Minimal code for multiple Analog Readings (8-bit or 10-bit)
 *  using ADC1, ADC2 and ADC3 on ATtiny85 (8MHz)
 * 
 * .qusw
******************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>

/*    	   *** PINOUT ***
 *             _______
 *        RES-|1  o  8|-VCC
 * ADC3 / PB3-|2     7|-PB2 / SCK / ADC1
 * ADC2 / PB4-|3     6|-PB1 / MISO
 *        GND-|4     5|-PB0 / MOSI
 *            |_______|	
 *	          ATtiny85
 *
 * WARNING : SCK and ADC1 share the same pin. 
 * This can be of concern when using ISP 
 */

void init_ADC(void){
	
/********NB**********
 ***PRESCALER
 *  ADC clock frequency must be between 50kHz and 200kHz
 *  ADC prescaler must be set accordingly
 *  F_CPU=8MHz ; Pre=64 >> 125kHz 
 *  A conversion takes 13 ADC clock cycles (idle)
 *  13/125kHz= 104 microseconds (~ 9,6kHz)
 * 
 ***RESOLUTION
 * ADLAR= 1 enables the Left-shift result presentation
 * 		ADCH |b9|b8|b7|b6|b5|b4|b3|b2|
 * 		ADCL |b1|b0|--|--|--|--|--|--|
 * This way, if no more than 8-bit precision is required
 * it is sufficient to read ADCH (ADC9 to ADC2)
 * Otherwise one must read ADCL first then ADCH
 *
 ***REF. VOLTAGE
 * By default REFS1=0; REFS2=0; in ADMUX register 
 * reference voltage is set then to VCC
 * 
 ***INTERRUPT
 * Setting ADIE=1 enables the ADC Conversion Complete Interrupt
 * so one can use the ADC Interrupt Servide Routine : ISR(ADC_vect){ }
 * 
 */
	cli();
	ADMUX = 	(1 << ADLAR) |    				// left-shift result for 8-bit readings
				(1 << MUX1);       				// start with ADC2 (PB4)
				
	ADCSRA = 	(1 << ADEN)  | //(1 << ADIE) | 	//enables ADC WHITHOUT interrupts
				(1 << ADPS2) | (1 << ADPS1) ;   //set Prescaler at 64
	sei();
}

void set_MPX(uint8_t ch){ 
	
	//ADCSRA &= ~(1<<ADEN); //switch off ADC
	
	ADMUX = (1 << ADLAR) | ((0x03) & ch);  //select ADC 1, 2 or 3  (ch%3)

	/* ADMUX = |REFS1|REFS2|ADLAR|REFS2|MUX3|MUX2|MUX1|MUX0| 
	 * 
	 * single-ended input (MUX3=0)
	 * VCC reference voltage (REFS1=REFS0=0)
	 * left-shifted presentation (ADLAR =1);
	 */
	 
	//ADCSRA|=(1<<ADEN); // re-enable ADC
}

uint8_t read_ADC_8bit(void){
		
		ADCSRA|=(1<<ADSC);  //start new conversion
		
		/* ADSC=1 as long as a conversion is in process */
		
		while (ADCSRA & (1<<ADSC));//wait until conversion is complete
		
	return ADCH;
}

uint16_t read_ADC_10bit(void){ 
	
		ADCSRA|=(1<<ADSC);  //start new conversion
		
		/* ADSC=1 as long as a conversion is in process */
		
		while (ADCSRA & (1<<ADSC)); //wait until conversion is complete
		
		/*NB: left-shifted data (ADLAR=1)
		 * ADCH |b9|b8|b7|b6|b5|b4|b3|b2|
		 * ADCL |b1|b0|--|--|--|--|--|--|
		 */
		 
	uint16_t lsb=(uint16_t)ADCL; // ! ADCL must be read first !
	uint16_t msb=(uint16_t)ADCH; 
	return ( (msb<<2)| (lsb>>6) );
}
