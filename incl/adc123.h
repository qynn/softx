/******************************************************************************
 * file    : adc123.h (header file for adc123.c)
 * target  : ATtiny85 (Int. Osc. @8MHz  w/ CKDIV8=1)
 * author  : .qusw (https://github.com/qusvv) 
 * license : GNU GPLv3
 * 
 * # # AnalogRead() for ATtiny85 # # 
 * 
 *  Minimal code for multiple Analog Readings (8-bit or 10-bit)
 *  using ADC1, ADC2 and ADC3 on ATtiny85 (8MHz)
 * 
 * .qusw
******************************************************************************/
#ifndef ADC123_H
#define ADC123_H


void init_ADC(void); //init ADC: single-ended input, Vref=VCC, left-shifted result 

void set_MPX(uint8_t ch); //select ADC channel (1, 2 or 3)
		
uint8_t read_ADC_8bit(void); // start conversion and read MSB
			
uint16_t read_ADC_10bit(void); //start conversion and read (MSB + LSB)

#endif /* ADC123_H */
