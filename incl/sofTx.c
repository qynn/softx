/******************************************************************************
 * file    : sofTx.c
 * target  : ATtiny85 (Int. Osc. @8MHz  w/ CKDIV8=1)
 * author  : .qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * # # Software Serial TX (8N1) # #
 * 
 * A minimal bit bang, interrupt driven UART (send only) for ATtiny85
 * Largely inspired by SoftwareUART from Jeroen Lodder (2011)
 * 
 * NB0: NewSoftSerial (Mikal Hart) and SoftwareSerial use respectively a tunedDelay() function 
 * written in assembly code and _delay_loop_2 from <utili/delay_basic.h> to adjust the timing 
 * between two consecutive bytes. This way, max. resolution is 4 CPU cycles (500ns @ 8MHz)
 * Comparatively, when using TIMER0 a 125ns resolution can be achieved by adjusting 
 * the compare match register (prescaler =1)
 * 
 * NB1: SoftwareSerial substracts about 15 cycles from bit delay to take into acount for
 * code execution. This is not done here. Rather, a switch structure is used to make sure 
 * delays between: 
 * - Start bit and first data bit.
 * - two consecutive data bits
 * - last data bit and stop bit
 * are all kept at pretty much the same value. 
 *
 * WARNING: use fuse bits [L:e2 H:df E:ff] (see makefile)
 * so that clock is NOT divided by 8 (CKDIV8=1) >> F_CPU= 8MHz 
 * 
 * .qusw
******************************************************************************/
#include "sofTx.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//Variables:
volatile uint8_t	TXstep	= 0;	// 8N1 step counter (0=PAUSE; 1=START; 2-9=DATA; 10=STOP)
volatile uint8_t 	TXdata = 0;		// Data byte
volatile uint16_t	t = 0;			// timer1 index @1kHz

void init_TX(void){ 		/* init TX on TIMER0 (CTC mode)*/
	
	cli(); 					//disable global interrupts
	DDRB	|=	(1<<TXpin);	//TXpin set as Output in DDRB (Data Direction Register)
	PORTB	|=	(1<<TXpin); //initialize at high state
	TCCR0A = 0;				// set entire TCCR0A register to 0
	TCCR0B = 0;				// same for TCCR0B
	TCNT0  = 0;				// initialize counter value to 0
	 
	 /* 
	 * fHz=115200 ; Pre = 1 
	 * OCR0A = [F_CPU/(fHz*Pre) -1] = [8*10^6/(115200) -1] ~ 68 (+/-1) >> 69 OK!
	 * !! NB !! : Compare Register MUST BE < 256 !!!
	 *
	 */
	 
	TCCR0B |= (1 << CS00);  // no prescaler >> RES@8MHz = 125ns 
	OCR0A = 69;			// see comment above
	TCCR0A |= (1 << WGM01); // turn on CTC mode
	TIMSK  |= (1 << OCIE0A);// enable timer interrupt
	
	/*NB: Timer Interrupt Mask Register
	TIMSK  |  -  |OCIE1A|OCIE1B|OCIE0A|OCIE0B|TOIE1|TOIE0|  -  |
	*/
	
	sei();					//enable interrupts
}

ISR(TIM0_COMPA_vect){		//Interrupt on Timer0
	
	switch(TXstep){
		//##########
		case 0:											    //IDLE
			TIMSK &= ~(1<<OCIE0A);							//disable timer
			TIMSK |= (1<<OCIE1A);    						//re-enable TIMER1
			break; 	//Wait until the next SEND request
		//##########
		case 1:												//START bit
			PORTB  &= ~(1<<TXpin);							//Pull down (LOW)
			TXstep++;									    //step increment
			break;	//Wait (1 bit) then goes to DATA Mode
		//##########
		case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:											//DATA Mode (8 bits)
				if( TXdata & 0x01) PORTB |= (1<<TXpin);		//Pull up (HIGH)
				else PORTB  &= ~(1<<TXpin);					//Pull down (LOW)
				TXdata >>= 1;								//Shift Data byte once to the right
				TXstep++;								//Increment byte counter						
				break;	//Wait (1bit)
		//##########
		case 10:											//STOP bit								
			PORTB |= (1<<TXpin);							//Pull up (HIGH)
			TXstep  = 0;									//Reset step counter
			break; // goes to PAUSE mode
		//##########
		default:
			TXstep 	= 10; // STOP bit (HIGH) then PAUSE by default
			break;
	}
	return;
}


void init_timer1(void){ /* Set up TIMER1 at fHz=1kHz (CTC mode) */
	
	/* re-initialize timer1 */
	cli(); 					// disable global interrupts
	TCCR1 = 0;				// set entire TCCR1 register to 0
	TCNT1  = 0;				// initialize counter value to 0	
	GTCCR = 0;				// make sure that PWM is disabled (PWM1B=0)
	 
	/* set compare Register */
	OCR1A = 249;
	/**NB**
	 * F_CPU = 8MHz/1 = 8MHz ( fuse bits [L:62 H:DF E:FF] )
	 * fHz=1kHz ; Pre = 32
	 * OCR1A = [F_CPU/(fHz*Pre) -1] = [8*10^6/(10^3*32) -1] = 249
	 * Compare Register (8-bit) MUST BE < 256 !!!
	 */
	 
	/* set prescaler */
	TCCR1 |= (1 << CTC1) |  	// clear timer on compare match (CTC mode)
			 (1 << CS11) | (1 << CS12);   					//32 prescaler
	//TCCR1 |= (1 << CS10) | (1 << CS11) | (1 << CS12);   	//64 prescaler
	//TCCR1 |= (1 << CS13);  								//128 prescaler

	TIMSK |= (1 << OCIE1A); // enable interrupts on TIMER1 (COMPA)
	
	/*Timer Interrupt Mask Register
	TIMSK  |  -  |OCIE1A|OCIE1B|OCIE0A|OCIE0B|TOIE1|TOIE0|  -  |
	*/
	
	sei();	//enable global interrupts
}

ISR(TIM1_COMPA_vect){
	t++;
}

void send_byte(uint8_t B){	    //1 data byte (or 1 ASCII caracter)
	while(TXstep > 0){
		//Wait until previous message has been sent
		}; 
		TXdata = B;					//copy data to be sent
		TXstep = 1; 				//START bit Case
		TIMSK 	&= ~(1<<OCIE1A);    //disable TIMER1 to avoid delays
		TIMSK 	|=  (1<<OCIE0A);	//re-enable TIMER0
		
		/*data byte will be sent from the Interrupt Service Routine
		 * and TXstep will go back to 0 when job is done
		 */
}

void send_int(int a){	
	uint8_t lgth=6; //16-bit integers up to 65535
	char buf[lgth]; //5 digits + nul character ('\0')
	char *str=&buf[lgth-1]; //now points to last character
	*str ='\0'; //nul character to terminate string
	do{
		char c = a % 10;
		*(--str)= c + '0'; //ascii digits start at '0'=48;
		a/=10;
	}while(a);
	
	send_str(str);
}
	
void send_str(char *s){
	uint8_t i= 0; //counter 
	
	send_byte('\0'); //send dummy byte to avoid errors on first character
	while ( (s[i] != '\0') & (i < STR_MAX) ) //end of string  	
	{
		send_byte(s[i]);
		i++;
	}
}



