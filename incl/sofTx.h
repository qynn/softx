/******************************************************************************
 * file    : sofTx.h 
 * target  : ATtiny85 (Int. Osc. @8MHz  w/ CKDIV8=1)
 * author  : .qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * 
 * # # Software Serial TX (8N1) # #
 * 
 * A minimal bit bang UART (send only) for ATtiny85
 * Largely inspired by SoftwareUART from Jeroen Lodder (2011)
 * 
 * WARNING: use fuse bits [L:e2 H:df E:ff] (see makefile)
 * so that clock is NOT divided by 8 (CKDIV8=1) >> F_CPU= 8MHz 
 * 
 * .qusw
******************************************************************************/
#ifndef SOFTX_H
#define SOFTX_H

#include <avr/interrupt.h>
#include <avr/io.h>

#define START_BIT 1
#define DATA_BIT 2
#define STOP_BIT 3

#define STR_MAX	20   // Maximum length for Strings

#ifndef TxPin
#define TXpin	PB1  // ATtiny85 : either PB0 or PB1
#endif

//#define SEND_MIDI
//#define SEND_CC

//MAIN FUNCTIONS
void init_TX(void);		 			// init software uart 

void init_timer1(void);				// init TIMER1 @ 1kHz

void send_byte(uint8_t B);			//send single byte (8N1 bit bang routine)

void send_int(int a);		// send integer (16-bit)
	
void send_str(char *s);		//Send string (multiple bytes)


#endif /* SOFTX_H */



