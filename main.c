/******************************************************************************
 * file    : main.c  (main program)
 * project : sofTx (Send-Only Software UART)
 * target  : ATtiny85 (Int. Osc. @8MHz  w/ CKDIV8=1)
 * author  : .qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * Short example to demonstrate the use of sofTx.c on ATtiny85
 * to interface with a Serial Port Monitor (e.g. minicom) 
 * through RS-232 (e.g. FTDI232 breakout board)
 * 
 * adc123.c allows to read all 3 ADCs with both 8-bit and 10-bit resolutions 
 *  
 * WARNING: use fuse bits [L:e2 H:df E:ff] (see makefile)
 * so that clock is NOT divided by 8 (CKDIV8=1) >> F_CPU= 8MHz 
 * 
******************************************************************************/ 

#include "main.h"

extern volatile uint16_t t; //elapsed time (ms) (TIMER1@1kHz)
uint16_t dt=1000; 			//timer delay in milliseconds
uint8_t ch=1;   			//MPX Channel : OFF,ADC1,ADC2,ADC3
uint8_t val8=0; 			//8-bit result
uint16_t val10=0; 			//10-bit result

int main(){
	
	init_TX();       	// init TX on TIMER0
	init_timer1();   	// set up TIMER1 at fHz=1kHz
	init_ADC();      	// setup Analog Read
	DDRB |= (1<<ledPin);// set PB0 as output
	
	while(1)
	{			
		if(t>dt)  //get values
		{
			PORTB |= (1<<ledPin);    // LED ON
			set_MPX(ch); 			//set multiplexer to channel
			read_ADC_8bit();		//dummy reading
			val8=read_ADC_8bit();   //8-bit reading
			val10=read_ADC_10bit();  //10-bit reading
		}
		if (t>2*dt) //print values
		{
			PORTB &= ~(1<<ledPin);      // LED OFF
			send_str(" >");
			send_byte(62);  	//ascii character ('>')
			send_str(" ch:_");
			send_int(ch);
			send_str("_ 8-bit:");
			fill8(val8); 		//adds spaces to fill 3 digits
			send_int(val8); 	//print 8-bit value
			send_str(" /vs/ 10-bit:");
			fill10(val10); 		//adds spaces to fill 4 digits
			send_int(val10); 	//print 10-bit value
			send_str(" . . \r");
			
		    t=0;				// restart time
		    if (ch++==3) ch=1;  //increment channel

		}
	}//while(1)	

}//main()	

void fill8(uint8_t b){ //hack for 3-digit fixed-width display
	uint16_t a = (uint16_t)b;
	if (a==0){a+=1;}
	while (a<100)
	{
		send_byte(32); //space
		a*=10;
	}
}

void fill10(uint16_t a){ //hack for 4 digit fixed-width display
	if (a==0){a+=1;}
	while (a<1000)
	{
		send_byte(32); //space
		a*=10;
	}
}
