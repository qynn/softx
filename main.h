/******************************************************************************
 * 
 * file    : main.h  (main header)
 * project : sofTx (Send-Only Software UART)
 * target  : ATtiny85 (Int. Osc. @8MHz  w/ CKDIV8=1)
 * author  : .qusw (https://github.com/qusvv)
 * license : GNU GPLv3
 * 
 * 
 * 
******************************************************************************/
#ifndef MAIN_H
#define MAIN_H

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#include "incl/sofTx.h"
#include "incl/adc123.h"

/*             
 *    	    *** PINOUT ***
 *             _______
 *      RESET-|1  o  8|-VCC
 * ADC3 / PB3-|2     7|-PB2 / SCK / ADC1
 * ADC2 / PB4-|3     6|-PB1 / MISO / sTX
 *        GND-|4     5|-PB0 / MOSI / LED
 *            |_______|	
 *	          ATtiny85
 *
 * !! WARNING : ADC1 (PB2) is shared with SCK !!
 * wiring a potentiometer on this pin can prevent 
 * avrdude to communicate properly with the device 
 * depending on the wiper's position
 * 
 */

//--------Pinout------------
#define ledPin PB0   //  PB0 : debug LED
#define potPin PB3   //  PB3: 10k linear Pot.
#define TXpin	PB1    //  PB1: sofTx pin (sTx)

void fill8(uint8_t b); //hack for 3 digit fixed-width display

void fill10(uint16_t a); //hack for 4 digit fixed-width display

#endif /*MAIN_H*/
