#####################################
# basic makefile for avr-gcc + avrdude
#  TARGET MCU : ATtiny85 (8MHz)
# largely inspired by fd0@kooeln.ccc.de
# ###################################

#target name
BIN=main
#Include directory
INCL=incl/
#C source files
#SRC=${BIN}.c ${INCL}*.c
SRC=main.c incl/adc123.c incl/sofTx.c

# MCU parameters
MMCU=attiny85
MCU=t85
PRG=usbasp
PORT=/dev/ttyACM0
#CHANGE F_CPU if CKDIV8 fuse bit is not default
F_CPU=8000000UL

# GCC options
CC=avr-gcc
OBJCOPY=avr-objcopy
CFLAGS= -g -Os -DF_CPU=${F_CPU} -mmcu=${MMCU} 
CFLAGS+= -std=gnu99 -Wall $(addprefix -I,$(INCL)) 

# for automatic dependencies
#DEPFLAGS = -MD -MP -MF .dep/$(@F).d
#CFLAGS+= -fshort-enums $(DEPFLAGS)
#-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

OBJS += $(SRC:.c=.o)
# $@ : name of the current target
# $< : name of the first prerequisite
# $@ : name of the current target
# $^ : list of all prerequisites  

# make load : to run avrdude
# make clean : to remove the compiled files

${BIN}.hex: ${BIN}.elf
	${OBJCOPY} -O ihex -R .eeprom $< $@
    
${BIN}.elf: ${OBJS}
	${CC} $(CFLAGS) -o $@ $^
    
load: ${BIN}.hex
	#CKDIV8 disabled
	avrdude -c ${PRG} -p ${MCU} -P ${PORT} -U flash:w:$< -v -U lfuse:w:0xe2:m -U hfuse:w:0xdf:m -U efuse:w:0xff:m

clean:
	rm -f ${BIN}.elf ${BIN}.hex ${OBJS}
	
	

	
